### Stylish portfolio
Landing page example.

- You can check this project here: https://chaba-portfolio.herokuapp.com/

### Clone your project
- Go to your computer's shell and type the following command with your SSH or HTTPS URL:
- `$ git clone git@gitlab.com:chaba3454/stylish-portfolio.git`
- You can copy a link to the Git repository through a SSH or a HTTPS protocol.

### Development
- Install dependencies
`$ npm install`
- If you want to start the live-reload server
`$ npm start` or `$ npm run watch` (it will crate '/dev' directory in RAM and accept all changes from 'source/')
- If you want to build dev version of the project 
`$ npm run dev` or `$ npm run development` (it will create 'dev/' directory in project root folder without live-reload)

### Production Build
- Install dependencies
`$ npm install`
- Run `$ npm run prod` or `$ npm run production` (it will create 'dist/' directory)
- If you want to check and serve 'dist/' folder, use **live-server** .
- To install **live-server** run `$ npm install -g live-server`
- After intall you can start it, go to 'dist/' folder and run `$ live-server`

### Webpack Plugins and Features
- Webpack-dev-server
- Babel
- Autoprefixer
- Postcss-loader
- File-loader
- Aglifyjs-webpack-plugin
- Aptimize-css-assets-webpack-plugin
- Mini-css-extract-plugin
- Versioning .js and .css
- HTML minification
