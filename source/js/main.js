const $ = window.$ = window.jQuery = require('jquery');
import 'slick-carousel';

//Slider
function mobileSlider() {  //Creating slider function
    $('#portfolio-slider').slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 3000,
        mobileFirst: true,
    });
} if(window.innerWidth < 992) {  //If size of the window < 992px slider function will start
    mobileSlider()
}
//Creating resize function
$(window).resize(function(){
    if(window.innerWidth < 992) { //If size of the window < 992px
        if(!$('#portfolio-slider').hasClass('slick-initialized')){ /*At resize action slider checks if # wasn't initiated
        it will start slider function*/
            mobileSlider();
        }

    }else{
        if($('#portfolio-slider').hasClass('slick-initialized')){ // But if # was initiated it will destroy slider
            $('#portfolio-slider').slick('unslick');
        }
    }
});

//Sidenav
$(document).ready(function(){
    $(".burger, .sidenav a").click(function(){
        $(".sidenav").toggleClass("display-nav");
        $(".burger").toggleClass("change");
    });
});

//Modal window
$("#click-me").click(function(){
    $(".modal").toggleClass("change-modal");
});
$(".modal-close-btn").click(function(){
    $(".modal").removeClass("change-modal");
});


//Appearing back to top button
const btn = $('#top-btn');

$(window).scroll(function() {
    if ($(window).scrollTop() > 200) {
        btn.addClass('change-btn');
    } else {
        btn.removeClass('change-btn');
    }
});


//Back to top button
$("#top-btn").click(function () {
    $('html, body').animate(
        {scrollTop:0}, '300');
});


//Reach the end of the page button
$(window).scroll(function(){
    if ($(window).scrollTop() == $(document).height() - $(window).height()) {
        btn.addClass("change-btn-red");
    }else {
        btn.removeClass("change-btn-red")
    }
});


//Smooth scroll
$('a[href*="#"]')
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function(event) {
        if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
            &&
            location.hostname == this.hostname
        ) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000, function() {
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(":focus")) {
                        return false;
                    } else {
                        $target.attr('tabindex','-1');
                        $target.focus();
                    };
                });
            }
        }
    });