const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackExcludeAssetsPlugin = require('html-webpack-exclude-assets-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

//Minify options
const minifyOptions = {
    html5                          : true,
    collapseWhitespace             : true,
    minifyURLs                     : false,
    minifyJS                       : true,
    removeAttributeQuotes          : true,
    removeComments                 : true,
    removeEmptyAttributes          : true,
    removeOptionalTags             : true,
    removeRedundantAttributes      : true,
    removeScriptTypeAttributes     : true,
    removeStyleLinkTypeAttributese : true,
    useShortDoctype                : true
};

module.exports = {
    mode: "production",
    entry: ['./source/js/main.js', './source/scss/main.scss'],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[contenthash].js',
    },
    optimization: {
        minimizer: [
            new TerserPlugin({
                parallel: true,
                terserOptions: {
                    ecma: undefined,
                    warnings: false,
                    parse: {},
                    compress: {},
                    mangle: true, // Note `mangle.properties` is `false` by default.
                    module: false,
                    output: null,
                    toplevel: false,
                    nameCache: null,
                    ie8: false,
                    keep_classnames: undefined,
                    keep_fnames: false,
                    safari10: false,
                },
            }),
            new OptimizeCSSAssetsPlugin({

            }),
        ],

    },
    module: {


        rules: [

            {
                test: /\.s?css$/,
                use: ['style-loader', MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader', 'postcss-loader'],
            },
            {
                test: /\.(gif|eot|woff|ttf|woff2)$/,
                loaders: 'ignore-loader',
            },
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader?cacheDirectory=true',
                    options: {
                        presets: [
                            ["@babel/preset-env", {
                                "exclude": ["transform-async-to-generator", "transform-regenerator"]
                            }]
                        ],
                        plugins: [
                            ["module:fast-async", { "spec": true }],
                        ]
                    }
                }
            },
            {
            	// If file size less than 'limit', it will be transformed in base64 format  
                test: /\.(png|jpg|gif|svg)$/i,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 8192,
                            fallback: 'file-loader',
                            name: '[name].[ext]',
                            outputPath: 'img'
                        }
                    }
                ]
            },
        ],
    },
    plugins: [

    	// Clean 'dist' folder
    	new CleanWebpackPlugin(['dist']),


    	// This plugin will take a template and add links to .css and .js files
    	new HtmlWebpackPlugin({
            minify: minifyOptions,
            template: './source/index.html',
            filename: './index.html'
        }),

        //----------------------------------------------------------------
    	// If u want to add main.js and main.css links in another html file
    	// here is an exapmle:
		//----------------------------------------------------------------
        // new HtmlWebpackPlugin({
        //     minify: minifyOptions,
        //	   If u don't want to add mani.js link in the file use excludeAssets option
        //     excludeAssets: [/main.*.js/], 
        //     template: './source/404.html',
        //     filename: './404.html'
        // }),


        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: '[name].[hash].css',
            chunkFilename:  '[id].[hash].css',
        }),

        //This will copy all files (except files in ignore option) from "source folder"
        new CopyWebpackPlugin([
            {from: './source', to:'', ignore: ['index.html', 'sass/*', 'js/*', 'css/*', '404.html','img/']}
        ]),
        new HtmlWebpackExcludeAssetsPlugin(),
    ],
};
