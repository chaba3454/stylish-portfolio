const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackExcludeAssetsPlugin = require('html-webpack-exclude-assets-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');


module.exports = {
    mode: "development",
    entry: ['./source/js/main.js', './source/scss/main.scss'],
    output: {
        path: path.resolve(__dirname, 'dev'),
        filename: '[name].js',
    },
    module: {
        rules: [
            {
                test: /\.(png|jpg|gif|svg)$/i,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 80000,
                            fallback: 'file-loader',
                            name: '[name].[ext]',
                            outputPath: 'img'
                        }
                    }
                ]
            },
            {
                test: /\.s?css$/,
                use: ['style-loader', MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader'],
            },
            {
                test: /\.(gif|eot|woff|ttf|woff2)$/,
                loaders: 'ignore-loader',
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(['dev']),
        new HtmlWebpackPlugin({
            template: './source/index.html',
            filename: './index.html'
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename:  '[id].css',
        }),
        new CopyWebpackPlugin([
            {from: './source', to:'', ignore: ['index.html', '404.html', 'main.js', 'js/*', 'img/']}
        ]),
        new HtmlWebpackExcludeAssetsPlugin(),
    ],
    devServer: {
        contentBase: '/dev',
        stats: 'errors-only'
    },
};
